﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;

namespace WebApplication2.Model.Entity
{
    public class User
    {
        public string Name { get; set; } = "DefaultName";
        public string Email { get; set; } = "DefaultEmail";
        public string Phone { get; set; } = "DefaultPhone";

    }
}
