using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WebApplication2.Model.Entity;

namespace WebApplication2.Pages
{
    public class RegistryModel : PageModel
    {
        public User user { get; set; }
        private bool flag = true; 
        public void OnGet()
        {
            if (flag) user=new User();
        }

        public void OnPost(User? user)
        {
            this.user = user;
        }
    }
}
